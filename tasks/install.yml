---
- name: "Base Installation: create Namespace if not present"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    name: '{{ cluster_prometheus_namespace }}'
    api_version: v1
    kind: Namespace

- name: "Base Installation: create clusterrole for prometheus operator"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    definition:
      apiVersion: rbac.authorization.k8s.io/v1
      kind: ClusterRole
      metadata:
        name: '{{ cluster_prometheus_namespace }}'
      rules:
      - apiGroups:
        - apiextensions.k8s.io
        resources:
        - customresourcedefinitions
        verbs:
        - '*'
      - apiGroups:
        - '{{ cluster_prometheus_apiGroup }}'
        resources:
        - alertmanagers
        - prometheuses
        - prometheuses/finalizers
        - alertmanagers/finalizers
        - servicemonitors
        - prometheusrules
        verbs:
        - '*'
      - apiGroups:
        - apps
        resources:
        - statefulsets
        verbs:
        - '*'
      - apiGroups:
        - ""
        resources:
        - configmaps
        - secrets
        verbs:
        - '*'
      - apiGroups:
        - ""
        resources:
        - pods
        verbs:
        - list
        - delete
      - apiGroups:
        - ""
        resources:
        - services
        - endpoints
        verbs:
        - get
        - create
        - update
      - apiGroups:
        - ""
        resources:
        - nodes
        verbs:
        - list
        - watch
      - apiGroups:
        - ""
        resources:
        - namespaces
        verbs:
        - get
        - list
        - watch

- name: "Base Installation: create service account for custom-prometheus-operator"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    name: '{{ cluster_prometheus_operator_serviceAccount }}'
    api_version: v1
    kind: ServiceAccount
    namespace: '{{ cluster_prometheus_namespace }}'

- name: create clusterrolebinding
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    definition:
      apiVersion: rbac.authorization.k8s.io/v1
      kind: ClusterRoleBinding
      metadata:
        name: "{{ cluster_prometheus_namespace }}"
      roleRef:
        apiGroup: rbac.authorization.k8s.io
        kind: ClusterRole
        name: "{{ cluster_prometheus_namespace }}"
      subjects:
      - kind: ServiceAccount
        name: "{{ cluster_prometheus_operator_serviceAccount }}"
        namespace: "{{ cluster_prometheus_namespace }}"


- name: "Base Installation: deploy prometheus operator"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    namespace: "{{ cluster_prometheus_namespace }}"
    definition:
      apiVersion: apps/v1
      kind: Deployment
      metadata:
        labels:
          k8s-app: '{{ cluster_prometheus_namespace }}-operator'
        name: '{{ cluster_prometheus_namespace }}-operator'
        namespace: "{{ cluster_prometheus_namespace }}"
      spec:
        replicas: "{{ cluster_prometheus_operator_size }}"
        selector:
          matchLabels:
            k8s-app: '{{ cluster_prometheus_namespace }}-operator'
        template:
          metadata:
            labels:
              k8s-app: '{{ cluster_prometheus_namespace }}-operator'
          spec:
            containers:
            - args:
              - --kubelet-service=kube-system/kubelet
              - --logtostderr=true
              - --config-reloader-image=quay.io/coreos/configmap-reload:v0.0.1
              - --prometheus-config-reloader=quay.io/coreos/prometheus-config-reloader:{{ cluster_prometheus_operator_version }}
              - --crd-apigroup={{ cluster_prometheus_apiGroup }}
              - --disable-auto-user-group
              image: quay.io/coreos/prometheus-operator:{{ cluster_prometheus_operator_version }}
              name: '{{ cluster_prometheus_namespace }}-operator'
              ports:
              - containerPort: 8080
                name: http
              resources:
                limits:
                  cpu: 200m
                  memory: 200Mi
                requests:
                  cpu: 100m
                  memory: 100Mi
              securityContext:
                allowPrivilegeEscalation: false
                readOnlyRootFilesystem: true
            securityContext:
              runAsNonRoot: true
            serviceAccountName: "{{ cluster_prometheus_operator_serviceAccount }}"

- name: "Base Installation: Create app-prometheus clusterrole"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    definition:
      apiVersion: rbac.authorization.k8s.io/v1beta1
      kind: ClusterRole
      metadata:
        name: "{{ cluster_prometheus_namespace_serviceAccount }}"
      rules:
        - apiGroups:
            - authentication.k8s.io
          attributeRestrictions: null
          resources:
            - tokenreviews
          verbs:
            - create
        - apiGroups:
            - authorization.k8s.io
          attributeRestrictions: null
          resources:
            - subjectaccessreviews
          verbs:
            - create
        - apiGroups: [""]
          resources:
          - nodes
          - services
          - endpoints
          - pods
          verbs: ["get", "list", "watch"]
        - apiGroups: [""]
          resources:
          - configmaps
          verbs: ["get"]
        - nonResourceURLs: ["/metrics"]
          verbs: ["get"]

- name: "Base Installation: Create clusterrole for grafana"
  k8s:
    verify_ssl: "{{ lookup('env','K8S_AUTH_VERIFY_SSL') }}"
    state: present
    definition:
      apiVersion: authorization.openshift.io/v1
      kind: ClusterRole
      metadata:
        creationTimestamp: null
        name: grafana
      rules:
        - apiGroups:
            - authentication.k8s.io
          attributeRestrictions: null
          resources:
            - tokenreviews
          verbs:
            - create
        - apiGroups:
            - authorization.k8s.io
          attributeRestrictions: null
          resources:
            - subjectaccessreviews
          verbs:
            - create
